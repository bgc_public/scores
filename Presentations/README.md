# Présentations

Dossier des présentations.

Nomenclature des fichiers: `{date au format YYYY-MM-DD}_{titre_presentation}.(pdf|pptx)`

Exemple: `2021-03-30_REVEL.pdf`
