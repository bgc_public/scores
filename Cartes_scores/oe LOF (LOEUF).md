# Name

oe LOF (observed/expected loss-of-function)
or
LOEUF (loss-of-function observed/expected upper bound fraction)

# Type

Score 

# Category

Functional

# Scale

Gene

# Usefull for 

* SNV
* Coding
* Nonsense
* Splice (acceptor / donor)

# Short description

LOEUF stands for the "loss-of-function observed/expected upper bound fraction." LOEUF is a continuous metric, focused on high-confidence, high-impact pLoF variants that describes a spectrum of tolerance to pLoF variation for each protein-coding gene in the human genome. 

# Orientation and range

* Range : 0 to 1 (ratio).
* Orientation : Low LOEUF scores indicate strong selection against predicted loss-of-function (pLoF) variation in a given gene, while high LOEUF scores suggest a relatively higher tolerance to inactivation.

# Methodology:

Identification of pLoF variants in a cohort of 125,748 individuals with exome sequence data (gnomad 2.1.1). Applying the loss-of-function transcript effect estimator (LOFTEE) which distinguishes high-confidence pLoF variants from annotation artefacts, 443,769 high-confidence pLoF variants were discovered, of which 413,097 fall on the canonical transcripts of 16,694 genes.
Aggregating across variants, 1,555 genes were found to have an aggregate pLoF frequency of at least 0.1% across all individuals in the dataset, and 3,270 genes have an aggregate pLoF frequency of at least 0.1% in any one population. 4,332 pLoF variants that are homozygous in at least one individual were identified. Further filtering and curation led to defining a set of 1,815 genes (2,636 high-confidence variants) that are likely to be tolerant to biallelic inactivation.

Based on a refined mutational model predicting expected levels of variation under neutrality, depletion of pLoF variation was detected by comparing the number of observed pLoF variants against the expectation derived from the gnomAD exome data. Contrary to the pLI, which is used as a dichotomous metric, this model, owing to its substantially increased sample size allows a direct assessment of the degree of intolerance to pLoF variation in each gene, using the continuous metric of the observed/expected ratio and to estimate a confidence interval around the ratio. 
The 90% upper bound of this confidence interval, was termed the loss-of-function observed/expected upper bound fraction (LOEUF).

# VCF description (VEP)

# Long description:

The previous dichotomization of pLI, although convenient for the characterization of a set of genes, disguises variability in the degree of selective pressure against a given class of variation and overlooks more subtle levels of intolerance to pLoF variation. The LOEUF metric provides a continuous measure of intolerance to pLoF variation, which places each gene on a spectrum of LoF intolerance.
The median observed/expected ratio is 48%, which indicates that most genes exhibit at least moderate selection against pLoF variation, and that the distribution of the observed/expected ratio is not dichotomous, but continuous. At current sample sizes, this metric enables the quantitative assessment of constraint with a built-in confidence value, and distinguishes small genes (for example, those with observed = 0, expected = 2; LOEUF = 1.34) from large genes (for example, observed = 0, expected = 100; LOEUF = 0.03), while retaining the continuous properties of the direct estimate of the ratio.

At one extreme of the distribution (first LOEUF decile aggregate observed/expected approximately 6%), are genes with a very strong depletion of pLoF variation including genes previously characterized as high pLI.

Note that the use of the upper bound means that LOEUF is a conservative metric in one direction: genes with low LOEUF scores are confidently depleted for pLoF variation, whereas genes with high LOEUF scores are a mixture of genes without depletion, and genes that are too small to obtain a precise estimate of the observed/expected ratio.

# Training set

From the exome sequence data of a cohort of 125,748 individuals (gnomad 2.1.1), 443,769 high-confidence pLoF variants were selected,  413,097 of which fall on the canonical transcripts of 16,694 genes.

# Validation

LOEUF is positively correlated with the occurrence of 6,735 rare autosomal deletion structural variants overlapping protein-coding exons identified in a subset of 6,749 individuals with whole-genome sequencing data in [Collins et al., 2020](https://www.nature.com/articles/s41586-020-2287-8) (r = 0.13; P = 9.8 × 10−68), based on sequence-resolved structural variants constructed from 14,891 genomes across diverse global populations (54% non-European) in gnomAD.

# Performance

In an independent cohort of 5,305 individuals with intellectual disability or developmental disorders and 2,179 controls, the rate of pLoF de novo variation in cases is 15-fold higher in genes belonging to the most constrained LOEUF decile, compared with controls, with a slightly increased rate (2.9-fold) in the second highest decile but not in others.

# Example

The 678 genes that are essential for human cell viability as characterized by CRISPR screens (Hart, T. et al. 2017) are also depleted for pLoF variation (mean LOEUF = 0.63) in the general population compared to background (18,519 genes with mean LOEUF = 0.964; t-test P = 9 × 10−71), whereas the 777 non-essential genes are more likely to be unconstrained (mean LOEUF = 1.34, compared to remaining 18,420 genes with mean LOEUF = 0.936; t-test P = 3 × 10−92).

# Schema

[See: The functional spectrum of pLoF impact](https://www.nature.com/articles/s41586-020-2308-7)

# Warnings

* Far from all possible pLoF variants in the human exome are observed at this sample size (eg. only half of all possible stop-gained variants). A substantial fraction of the remaining variants are likely to be heterozygous lethal, whereas others will exhibit an intermediate selection coefficient.

* It should also be noted that these metrics primarily identify genes undergoing selection against heterozygous variation, rather than strong constraint against homozygous variation45. The power of the LOEUF metric is affected by gene length, with approximately 30% of the coding genes in the genome still insufficiently powered for detection of constraint even at the scale of gnomAD.

# Sources:

[Karczewski et al. 2020](https://www.nature.com/articles/s41586-020-2308-7)


