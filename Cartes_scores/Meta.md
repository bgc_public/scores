# Name

Meta (MetaLR or MetaSVM)

# Version

1

# Type

Metascore

# Category 

Ensemble

# Scale

gene

# Thematic

Constit

# Usefull for

* SNV
* Coding
* Missense

# Short description

MetaLR uses logistic regression or support vector machine to integrate nine independent variant deleteriousness scores and allele frequency information to predict the deleteriousness of missense variants. 

# Orientation and range

MetaLR :
* Range : 0 -> 1
* Cut-off : 0.5
* Orientation : Tolerated < 0,5 < damaging

MetaSVM :
* Range : -2 -> 3
* Cut-off : 0
* Orientation : Tolerated < 0 < damaging

# Methodology 

To construct our own ensemble scores, we used these nine scores (SIFT, PolyPhen-2, GERP++, MutationTaster, Mutation Assessor, FATHMM, LRT, SiPhy and PhyloP) for all potential human nsSNVs collected in the dbNSFP database, along with the MMAF observed in diverse populations of the 1000 Genomes project (32), as our component scores. After imputing missing values for observations in training dataset, we employed SVM and LR algorithms for computing these two ensemble scores and evaluated three kernel functions (linear, radial and polynomial kernel) for the SVM approach, all based on default parameters. Variants are classified as 'tolerated' or 'damaging'; a score between 0 and 1 is also provided and variants with higher scores are more likely to be deleterious. 

# VCF description (VEP)

`##MetaLR_pred=(from dbNSFP) Prediction of our MetaLR based ensemble prediction score,"T(olerated)" or "D(amaging)". The score cutoff between "D" and "T" is 0.5. The rankscore cutoff between "D" and "T" is 0.81113.`

`##MetaLR_rankscore=(from dbNSFP) MetaLR scores were ranked among all MetaLR scores in dbNSFP. The rankscore is the ratio of the rank of the score over the total number of MetaLR scores in dbNSFP. The scores range from 0 to 1.`

`##MetaLR_score=(from dbNSFP) Our logistic regression (LR) based ensemble prediction score, which incorporated 10 scores (SIFT, PolyPhen-2 HDIV, PolyPhen-2 HVAR, GERP++, MutationTaster, Mutation Assessor, FATHMM, LRT, SiPhy, PhyloP) and the maximum frequency observed in the 1000 genomes populations. Larger value means the SNV is more likely to be damaging. Scores range from 0 to 1.`

`##MetaSVM_score: Our support vector machine (SVM) based ensemble prediction score, which incorporated 10 scores (SIFT, PolyPhen-2 HDIV, PolyPhen-2 HVAR, GERP++, MutationTaster, Mutation Assessor, FATHMM, LRT, SiPhy, PhyloP) and the maximum frequency observed in the 1000 genomes populations. Larger value means the SNV is more likely to be damaging. Scores range from -2 to 3 in dbNSFP.`

`##MetaSVM_rankscore: MetaSVM scores were ranked among all MetaSVM scores in dbNSFP. The rankscore is the ratio of the rank of the score over the total number of MetaSVM scores in dbNSFP. The scores range from 0 to 1.`

`##MetaSVM_pred: Prediction of our SVM based ensemble prediction score,"T(olerated)" or "D(amaging)". The score cutoff between "D" and "T" is 0. The rankscore cutoff between "D" and "T" is 0.82257.`

# Long Description

MetaSVM and MetaLR are based on 10 component scores (SIFT, PolyPhen-2 HDIV, PolyPhen-2 HVAR, GERP++, MutationTaster, Mutation Assessor, FATHMM, LRT, SiPhy, PhyloP) and the maximum frequency observed in the 1000 genomes populations.

Training dataset is composed of 14 191 deleterious mutations as true positive (TP) observations and 22 001 neutral mutations as true negative (TN) observations, all based on the Uniprot database.

Note that the TN observations contain both common [maximum minor allele frequency (MMAF) >1% in diverse populations of the 1000 Genomes project] and rare (MMAF ≤1%) variants to ensure the generalizability of our model.

# Training set

14 191 TP + 22 001 TN = 36 192 Total from Uniprot database.

# Testing sets 

* Set I : 120 TP from Nature Genetics (54 genes/49 diseases) + 124 TN from the CHARGE project = 244 Total 
Deleterious mutations reported to cause Mendelian diseases, neutral mutations with a MMAF >1% in 2269 exomes (CHARGE), not reported in the 1000 Genomes Project or Uniprot and with Hardy–Weinberg exact test P-value > 0.01.

* Set II (benchmark): 6279 TP + 13 240  TN = 19 519 Total derived from VariBench dataset II (2013) (mutations overlapping with the training set were removed).

* Set III (evaluation on rare variants): 10 164 rare neutral mutations (singletons) (CHARGE project).

# Performance

* Separation of TP and TN variants : 
- MetaLR : set 1 AUC = 0.92 and 0.94 for set I & set II resp. MCC = 0.68 and 0.71 for set I & set II resp. 
- MetaSVM : AUC = 0.91 and 0.93 for set I & set II resp. MCC = 0.66 and 0.7 for set I & set II resp. 

* Rare Variants :
- MetaLR : TNR = 0.84 
- MetaSVM : TNR = 0.85

# Exemple

# Source

[dbNSFP](https://sites.google.com/site/jpopgen/dbNSFP)

[publication](https://academic.oup.com/hmg/article/24/8/2125/651446)

