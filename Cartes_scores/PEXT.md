# Name

PEXT (Proportion EXpressed across Transcripts)

# Version
1.0

# Useful for

Estimating how much an exonic region affected by the variant has evidence of expression accross tissues.

# Short description
The pext score, now integrated in the gnomAD database, summarizes isoform expression values accross tissues to allow for quick visualization of the expression status of exonic regions across tissues.


# Orientation and range

PEXT score ranges from 0 to 1. Expression of an exonic region can be estimated with pext values as follows: low (pext<0.1), medium (0.1 ≤ pext ≤ 0.9) and high (pext > 0.9) expression

# Methodology

Isoform-level quantifications were calculated (RSEM) from 11,706 tissue samples from the GTEx v7 dataset. The median expression of a transcript across tissue samples is calculated to create a summary isoform/tissue expression matrix. This matrix is used to define an expression of a given base as the sum of the expression of all transcripts that touch that base (raw ext score). The raw ext scores are calculated for every available tissue and then normalized to the total expression of the gene on which the variant is found : this is carried out by dividing the ext value with the sum of the expression of all transcripts per tissue in transcripts per million (TPM). The resulting pext score can be interpreted as the proportion of the total transcriptional output from a gene that would be affected by a given variant. The pext score is calculated for every possible SNV and every possible consequence. 

![Overview of the pext methodology](PEXT_flow.png)

# VCF description (VEP)


# Long Description
Our capacity to identify genetic variation exceeds our ability to interpret their functional and pathogenic impact. As such, a major challenge in medical genetics is to identify variants causal for the studied disease among a large background of neutral polymorphisms and non-causal variants. This is further complicated by the existence of disruptive (LoF) variants in known disease genes found in apparently healthy individuals (gnomAD database). In each patient, we can find many genetic variants that are rare, potentially functional, but not actually pathogenic for the disease under investigation. One of the possible explanations is isoform diversity due to alternative mRNA splicing, which enables exons to be expressed at different levels across tissues. These expression differences mean that variants in different regions of a gene can have different phenotypic outcomes depending on the isoforms they affect. 

The proposed PEXT score (proportion expressed across transcripts) is a transcript-level annotation metric which quantifies isoform expression for variants and integrates results from transcriptome sequencing experiments into clinical variant interpretation. This metric successfully discriminates between near-constitutive and low expression levels, which are useful for prioritizing and filtering variants, respectively.


# Exemple

[Site](https://github.com/macarthur-lab/gnomad-docs/blob/master/docs/pext.md)


# Source and useful links

[Publication](https://www.nature.com/articles/s41586-020-2329-2)

[Source code](https://github.com/macarthur-lab/tx_annotation)

[PEXT scores for each possible SNV for GTEX tissues](https://gnomad.broadinstitute.org/downloads#v2-pext)

Necessary files for pext annotation with the Human Brain Development Resource (HBDR) fetal brain dataset are available using  [gsutil](https://cloud.google.com/storage/docs/gsutil) - gs://gnomad-public/papers/2019-tx-annotation/data/HBDR_fetal_RNaseq

More information [here](https://github.com/macarthur-lab/tx_annotation)




