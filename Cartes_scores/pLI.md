# Name

pLI :  probability of being loss-of-function (LoF) intolerant

# Version

1

# Type 

Score

# Category 

Functionnal

# Scale

Gene

# Thematic 

Constit

# Usefull for 

* SVN
* Indels
* Coding
* PTV (Protein truncating variants)

# Short description

The pLI score indicates the tolerance of a gene to a loss of function based on the number of high confidence PTV (eg. frameshift, splice donor, splice acceptor, and stop-gained) referenced for this gene in a control database (ExAC/GnomAD databases).
This number is weighted by the size of the gene and the sequencing coverage.

# Orientation and range

* Range : from 0 (most tolerant) to 1 (most intolerant)
* Usual cut-off value for considering a gene as a  highly likely candidate : 0.9.


# Methodology:
(Lek et al. 2016)

Deleterious variants are expected to have lower allele frequencies than neutral ones, due to negative selection. This allows inference of the degree of selection against specific functional classes of variation. However, allele frequencies observed in ExAC-scale samples are skewed by mutation rate, with more mutable sites less likely to be singletons. Non-uniform mutation rate across functional classes were also corrected  by creating a mutability-adjusted proportion singleton (MAPS) metric. This metric reflects strong selection against predicted PTVs, as well as missense variants predicted by conservation-based methods to be deleterious.

The deep ascertainment of rare variation in ExAC allows us to infer the extent of selection against variant categories on a per-gene basis by examining the proportion of variation that is missing compared to expectations under random mutation. Conceptually similar approaches have been applied to smaller exome datasets, but have been underpowered, particularly when analyzing the depletion of PTVs. We compared the observed number of rare (MAF < 0.1%) variants per gene to an expected number derived from a selection neutral, sequence-context based mutational model.

We quantified deviation from expectation with a Z score, which for synonymous variants is centered at zero, but is significantly shifted towards higher values (greater constraint) for both missense and PTV (Wilcoxon p < 10e-50).

To reduce confounding by coding sequence length for PTVs, we developed an expectation-maximization algorithm using the observed and expected PTV counts within each gene to separate genes into three categories: null (observed ≈ expected), recessive (observed ≤50% of expected), and haploinsufficient (observed < 10% of expected). This metric – the probability of being loss-of-function (LoF) intolerant (pLI) – separates genes of sufficient length into LoF intolerant (pLI ≥0.9, n=3,230) or LoF tolerant (pLI ≤0.1, n=10,374) categories.

# VCF description (VEP)

`ExAC_pLI: "the probability of being loss-of-function intolerant (intolerant of both heterozygous and homozygous lof variants)" based on ExAC r0.3 data.`

`ExAC_pRec: "the probability of being intolerant of homozygous, but not heterozygous lof variants based on ExAC r0.3 data`

`ExAC_pNull: "the probability of being tolerant of both heterozygous and homozygous lof variants based on ExAC r0.3 data.`

# Long Description


# Training set

7,404,909 high quality (HQ) variants from the ExAC r0.3 dataset, comprising 3,230 highly LoF-intolerant genes.

# Testing sets 

ClinGen haploinsufficient gene list. 18,000 genes that had metric for the pLI and the p(HI) score (probability  of  being  haploinsufficient, (Huang  et  al. 2010).

# Performance

Among the 18 000 genes in the ClinGen haploinsufficient gene list, there were:
148 genes considered haploinsufficient by ClinGen, 109 of which with a pLI ≥ 0.8. and 51 with a p(HI) ≥0.8. 80% of those (n = 41) also had pLI ≥ 0.8.

3,175 genes with a pLI  ≥  0.9, 613  with a p(HI)  ≥  0.9. 
3,878 genes with a pLI  ≥  0.8 ; 1,061 with a p(HI)  ≥  0.8. 

# Example

The proportion of genes that are very likely intolerant of loss-of-function variation (pLI ≥ 0.9) is highest for ClinGen haploinsufficient genes, and stratifies by the severity and age of onset of the haploinsufficient phenotype. Genes essential in cell culture and dominant disease genes are likewise enriched for intolerant genes, while recessive disease genes and olfactory receptors have fewer intolerant genes.

# Schema

[Lek et al., 2016 - Figure 3b](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5018207/)

# Warnings

* ExaC dataset is deprecated ; gnomAD_pLI, gnomAD_pRec and gnomAD_pNull scores have been developd based on gnomAD 2.1 data.


* Highlighted by Ziegler et al., 2019

The pLI score is not suited for the identification of genes involved in autosomal recessive disorders, as PTVs in these genes may be frequent in the general population due to the lack of consequences in heterozygous carriers.

Some deleterious PTVs occurring in genes responsible for diseases that do not modify the ability of individuals to survive to adulthood and reproduce may be frequent in the GnomAD cohort.

Variants occurring in splice donor or acceptor sites are regarded as PTV even though their true effect on splicing has not been tested. Those could either have no significant effect on splicing or could lead to a splice variant or different protein isoform without loss‐of‐function.

There is a possibly high prevalence of specific disorders affecting individuals chosen as controls (eg. individuals affected with schizophrenia enrolled as “healthy” controls leading to an increased frequency of PTVs in genes predisposing to schizophrenia, which leads to a decrease of their pLI scores).

The pLI score should be used with caution when interpreting missense variants. Without a functional assay, it is indeed difficult to determine at the protein level whether a missense variant will result in a loss or a gain of function.

Particular attention must also be paid to variants resulting in a frameshift or in an early stop codon occurring in the last exon or in
the C‐terminal part of the penultimate exon. In such cases, the transcript generally escapes the nonsense‐mediated RNA decay and the variant does not result in haploinsufficiency. On the contrary, the residual truncated protein may behave like a gain‐in‐function mutant.

Finally, it is sometimes impossible to determine why a gene has a very low pLI score even though the pathogenicity of its haploinsufficiency has been clearly demonstrated (the associated disease might be too severe to be present in individuals from control databases).

# Sources:

[Original Publication](https://pubmed.ncbi.nlm.nih.gov/27535533/)

[Warnings](https://pubmed.ncbi.nlm.nih.gov/30977936/)


