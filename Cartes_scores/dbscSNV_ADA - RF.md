# Name

dbscSNV_ADA (Adaboost)
dbscSNV_RF (Random forests)

# Version

1

# Type 

Metascore

# Category 

Functional

# Scale

Nucleotide

# Thematic 

Constit

# Useful for 

* Splice altering SNV

# Short description

Ensemble scores developed to specifically detect splice-altering SNV and constructed via adaptative boosting or random forest learning method (Adaboost and random forests R packages, respectively). They both comprise 7 prediction scores (notably Position Weight Matrix & MaxEntScan), 2 conservation scores (PhyloP46 placental & primate), as well as CADD_raw & CADD_phredd.

# Orientation and range
(For both ADA and RF)

* Range: 0 to 1
* Orientation: the higher the value, the higher the probability to affect splicing.
* Suggested Cut-off: 0.6
# Methodology:

ROC analyses with 10-fold cross-validation were performed to tune the ensemble scores. The cutoff values were defined as the score variation above which the results should be considered positive. Sensitivity and specificity across the whole spectrum of possible cutoff values were calculated by comparing the predictive results with the ‘true’ results for a series of cutoff values from the minimum to the maximum, and all pairs were plotted to form a ROC curve. 

For each score variation, the AUC values were used to identify the optimum cutoff value that maximized accuracy in the cross-validation training set to calculate the accuracy, sensitivity, specificity, positive predictive value (PPV) and negative predictive value (NPV) in the corresponding test set. 

The ‘real boost’ algorithm in AdaBoost was used. For both ensemble methods, the relative importance of each individual score was measured by the frequency of a score selected for boosting for AdaBoost and the mean decrease in accuracy for random forests.

# VCF description (VEP)

` ## ADA_score: ensemble prediction score based on ada-boost. Ranges 0 to 1. The larger the score the higher probability the scSNV will affect splicing. The suggested cutoff for a binary prediction (affecting splicing vs. not affecting splicing) is 0.6.`

` RF_score: ensemble prediction score based on random forests. Ranges 0 to 1. The larger the score the higher probability the scSNV will affect splicing. The suggested cutoff for	a binary prediction (affecting splicing vs. not affecting splicing) is 0.6.`

# Long Description

Both ensemble scores computed using AdaBoost and random forests are the probabilities of a variant being splice-altering. As such, the score does not reflect the effect size or how damaging the variant is, but rather the confidence that it alters splicing. When applying the models to the human genome and data from COSMIC, a variant was defined to be splice-altering if either its ada_score or rf_score was higher than 0.6, the optimum cutoff point identified in the evaluation analysis. For exploratory purposes, one can choose a lower cutoff point (eg. 0.5) to increase sensitivity at the expense of specificity.
# Training set

Splice-altering variants from three databases: the HGMD Professional Version 2013.1, with 13 000 mutations with consequences for mRNA splicing; the SpliceDisease database with 2337 splicing mutation-disease entries, including 303 genes and 370 human diseases, the Database for Aberrant Splice Sites (DBASS), with 577 and 307 records of mutation-induced and disease-causing aberrant 5′ and 3′ splice sites, respectively). 
Negative variants were retrieved from the 1000 Genomes Project phase 1 data (18), including the genomes of 1092 individuals from 14 populations that can be used as controls.

# Testing sets 

Positive group : 1164 unique splice-altering scSNVs within 408 genes from three databases, HGMD, the SpliceDisease and DBASS. 
Negative group : 1795 scSNVs within 1447 genes from the 1000 Genomes Project phase 1 data. 
Total sample size was 2959 (within 1804 genes), 1682 & 1277 within the 5′ and 3’ splicing consensus regions resp. ;  1881 intronic, 1078 were exonic.


# Performance
 
AdaBoost (7 initial prediction scores) AUC = 0.963
Random Forests (7 initial prediction scores) AUC = 0.964
AdaBoost (all 11 scores) AUC = 0.977
Random Forests (all 11 scores) AUC = 0.978

# Example

Additional validation performed on a test set curated from [Houdayer et al. 2012]( https://pubmed.ncbi.nlm.nih.gov/22505045/): 45 scSNVs with experimentally validated splicing outcomes, 19 being positive and 26 negative.
With a cut-off value of 0.6, all 19 positive scSNVs were correctly predicted by AdaBoost (six false positives, sensitivity = 1 and specificity = 0.77 ; ac-curacy=0.87). 
For random forests, there was one missing score for a negative scSNV. All 19 positive scSNVs were correctly predicted, (three false positives, sensitivity = 1 and specificity = 0.88 ; accuracy=0.93).

# Schema

# Warnings

* Among all the splice-altering variants recruited, not all were validated as actually deleterious.
* The analysis is limited to variants within splicing consensus regions.
* The different outcomes of splice-altering variants were not distinguished: a score above the cut-off only indicates that there is an effect, not what is it.

# Sources:

[Original Publication](https://academic.oup.com/nar/article/42/22/13534/2411339)
