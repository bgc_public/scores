# Name

S-CAP (Splicing Clinically Applicable Pathogenicity Score)

# Version

1.0

# Type

Metascore

# Category 

Functionnal

# Scale

Nucleotide 

# Thematic

Constit

# Usefull for

* Splicing 

# Short description

S-CAP splicing-specific pathogenicity score using a machine learning tool to integrates features derived from the splicing literature with measures of variant, exon, and gene importance.

# Orientation and range 

In progress :

Thresholding with the rawscore

Based on the region field pick the corresponding threshold (specified at the bottom) to perform pathogenicity classication

Thresholding with the scap sensscore

Variants with a sensscore <= 95 are considered pathogenic for all splicing regions. 

Raw score v. Sensitivity score

The advantage of the sensitivity score is that the same thresholding (<= 95) can be used regardless of which region the single nucleotide variant is from but due to the limited size of the test set many single nucleotide variants will get grouped together with the same sensitivity score. The advantage of the raw score is that it is more granular but different thresholds need to be used per region and the scores are not as interpretable.

How to use the 6 score in the INFO column

If the splicing region for a single nucleotide variant is 3intronic, exonic, 5extended or 5intronic you should use the rawscore or sensscore. If the single nucleotide variant is 3 or 5 core and heterozygous then you should use the rawscore_dom or sensscore_dom. If the single nucleotide variant is 3 or 5 core and recessive or compound heterozygous then you should use the rawscore_rec or senscore_rec.


# Methodology = {Publications material and method}

# VCF description

`# region: the splicing regions in which the single nucleotide variant lies (3intronic, 3core, exonic, 5core, 5extended, 5intronic)`

`# rawscore: the S-CAP score output by the Gradient Boosting tree model. This score is not included for 3core and 5core variants`

`# sensscore: a transformed S-CAP score quantifying the sensitivity at the raw score on the pathogenic test set. This score is not included for 3core and 5core variants.`

`# rawscore_dom: the S-CAP score for the 3core DOM and 5core DOM variants. Score is not included for variants from all other regions.`

`# sensscore_dom: Transformed S-CAP score to quantify the sensitivity of the rawscore_dom on the pathogenic test set.`

`# rawscore_rec: the S-CAP score for the 3core REC and 5core REC variants. Score is not included for variants from all other regions.`

`# senscore_rec: Transformed S-CAP score to quantify the sensitivity of the rawscore_rec on the pathogenic test set`

# Long Description

S-CAP combines sequenced-based features, evolutionary conservation, and existing metrics including SPIDEX, CADD, and LINSIGHT. S-CAP is comprised of 6 separate models, each designed to predict the pathogenicity of rare SNV in a different splicing region.

# Training set = {Training set}
# Testing set = {Testing set}
# Performance = {AUC, SN, SP}
# Exemple = {Alamut screenshot, genome browser screenshot}
# Source = {Online tool, publication}

[Tool](http://bejerano.stanford.edu/scap/)

[Publication](https://www.nature.com/articles/s41588-019-0348-4.epdf?author_access_token=ACUUwGrgCdYpfeekk8hGPtRgN0jAjWel9jnR3ZoTv0OzLhWWBSr9gEXSE0_wq_Jh5His4BFP8OvH2DENs00Y0U_hJ1DVLL5ITJdqLuAOy_DyDNfQ3CeYdhJt2KSMiCPh2iCCzkRjr7DonppabWzYew%3D%3D)
